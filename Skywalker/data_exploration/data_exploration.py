%load_ext autoreload
%autoreload 2
import cnextlib.dataframe as cd
import pandas as pd 
from udf import *
df_housing = cd.DataFrame(pd.read_csv("data/housing_data/data.csv"))
# df_housing.drop(columns=['Alley'], inplace=True)
df_housing['LotFrontage'] = df_housing['LotFrontage'].fillna(method='ffill')
# df_housing.loc[df_housing['Id']==2, ['MSZoning']] = 'RS'
# df_housing.iloc[3, 6] = 'Grvl'
# df_housing.at[10, 'LotShape'] = 'IR2b'
df_housing = df_housing[df_housing.columns[:5]]
df_housing.describe(include='all')
df_housing.shape
df_train = pd.read_csv('Amex Data/small_train_data.csv')
# df_train1 = df_train[df_train.columns[:5]]
from dataqualityreport import dqr_table
dqr_table(df_housing.df)
import matplotlib.pyplot as plt
import matplotlib_inline
import seaborn as sns
matplotlib_inline.backend_inline.set_matplotlib_formats('svg')
# sns.histplot(df_housing['LotFrontage'])
plt.figure(figsize=(4,2))
sns.boxplot(data=df_housing, x='LotFrontage')
plt.xlabel("")
plt.ylabel("")
# sns.set(rc={'figure.figsize':(10,10)})
plt.show()
# exist(df_hous)
# df_name = f'{df_housing=}'.split('=')[0]
# df_name in globals()
# f'{df_housing=}'.split('=')[0]
type(df_housing).__module__, type(df_housing).__qualname__
electric = cd.DataFrame("data/Electric_Production.csv")
# electric.set_index('DATE', inplace=True)
electric.iloc[electric.shape[0]-2:,]
df_antibiotics = pd.read_csv("data/bokeh/antibiotics.csv")
df_antibiotics['Test'] = range(16)
# df_antibiotics.drop(columns=['Test'], inplace=True)
# cd.set_updated([df_antibiotics])
import jupyter_client; jupyter_client.kernelspec.find_kernel_specs()
df_housing.loc[df_housing['Alley']].iloc[0:50]
from cnextlib import udf
# udf.clear_udfs()
udf.registered_udfs