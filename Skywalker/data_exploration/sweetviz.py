import sweetviz as sv
import pandas as pd
train_df = pd.read_csv('http://cooltiming.com/SV/train.csv')
test_df = pd.read_csv('http://cooltiming.com/SV/test.csv') 
comparison_report = sv.compare([train_df,'Train'], [test_df,'Test'], target_feat='Survived')
comparison_report.show_notebook()