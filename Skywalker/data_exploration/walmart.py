%load_ext autoreload
%autoreload 2
import cnextlib as cl
import pandas as pd
import random
import simplejson as json

# img_urls = ['https://i5.walmartimages.com/asr/84835fa1-d3cd-4ca5-ba74-ad67d8b65812.4f2e48b02f23175e273bc1c9df42131d.jpeg',
#             'https://i5.walmartimages.com/asr/3ea8e6f3-bff0-4195-a6bf-41f4e86dc73d_1.5e8cf62f3e0428161a2279ae13351afd.jpeg', 
#             'https://i5.walmartimages.com/asr/6b8a7af8-0c49-46cd-893f-2f1138b83a8d.d015ca263d64ad5bd9bcbcbea1448314.jpeg']
img_urls = ['https://i5.walmartimages.com/asr/84835fa1-d3cd-4ca5-ba74-ad67d8b65812.4f2e48b02f23175e273bc1c9df42131d.jpeg' for i in range(300)]
data = {
    'pred': [random.randint(0,1) for i in range(len(img_urls))],
    'true': [random.randint(0,1) for i in range(len(img_urls))],
    'image': img_urls,
    'correct_type': [json.dumps({"options": ["chairs and table", "a chair", "two chairs"], "input": 0}) for i in range(len(img_urls))],
    'is_correct': [json.dumps({"name": "good", "input": False}) for i in range(len(img_urls))],
    'comment': [json.dumps({"input": ""}) for i in range(len(img_urls))]
}
df = pd.DataFrame(data=data)
df['image'] = df['image'].astype('url/png')
df['correct_type'] = df['correct_type'].astype('input/selection')
df['is_correct'] = df['is_correct'].astype('input/checkbox')
df['comment'] = df['comment'].astype('input/text')
from cnextlib import utils as cu
# df1 = df.copy()
df2 = cu.normalize_inputable_dataframe(df)