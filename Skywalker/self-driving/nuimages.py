%matplotlib inline
%load_ext autoreload
%autoreload 2
from nuimages import NuImages

nuim = NuImages(dataroot='data/nuimages', version='v1.0-mini', verbose=True, lazy=True)
nuim.category[0]
nuim.table_names
sample_idx = 0
sample = nuim.sample[sample_idx]
sample
key_camera_token = sample['key_camera_token']
print(key_camera_token)
nuim.render_image(key_camera_token, annotation_type='all',
                  with_category=True, with_attributes=True, box_line_width=-1, render_scale=5)
object_tokens, surface_tokens = nuim.list_anns(sample['token'])
nuim.render_image(key_camera_token, with_category=True, object_tokens=[object_tokens[0]], surface_tokens=[surface_tokens[0]])
import matplotlib.pyplot as plt

semantic_mask, instance_mask = nuim.get_segmentation(key_camera_token)

plt.figure(figsize=(32, 9))

plt.subplot(1, 2, 1)
plt.imshow(semantic_mask)
plt.subplot(1, 2, 2)
plt.imshow(instance_mask)

plt.show()