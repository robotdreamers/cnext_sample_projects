from diagrams import Diagram
from diagrams.aws.compute import EC2
from diagrams.aws.database import RDS
from diagrams.aws.network import ELB

with Diagram("Grouped Workers", show=False, direction="TB") as diag:
    ELB("lb") >> [EC2("poopy"),
                  EC2("loopy"),
                  EC2("toopy"),
                  EC2("soopy"),
                  EC2("yoopy")] >> RDS("events")
# ## Proposed Architecture 
# Small text works better here
diag