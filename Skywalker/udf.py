# from cnextlib import udf_manager
from cnextlib.udf_manager import register_udf, Config, View, OutputType, Position, Shape, Location, View, clear_udfs
import matplotlib.pyplot as plt
import matplotlib_inline
matplotlib_inline.backend_inline.set_matplotlib_formats('svg')
import seaborn as sns
import plotly.express as px
import plotly.io as pio
pio.renderers.default = "jupyterlab"
import inspect
MAX_POINT_COUNT = 1000

clear_udfs()

@register_udf(Config("histogram", OutputType.IMAGE, {Location.TABLE_HEADER: View(Position(1, 0)), Location.SUMMARY: View(Position(0, 1))}))
def histogram(df, col_name):
    print('Get %s for column %s... '%(inspect.stack()[0][3], col_name), end='')
    if df[col_name].dtypes not in ["object"]:
        plt.figure(figsize=(8,4))
        sns.histplot(x=df[col_name], color="#3283FE")
        plt.xlabel(""); plt.ylabel(""); plt.show()
    else:
        value_counts = df[col_name].value_counts()
        sns.barplot(x=value_counts.index, y=value_counts.values, color="#3283FE")
    print('Done!')
    
@register_udf(Config("quantile", OutputType.IMAGE, {Location.SUMMARY: View(Position(0, 0)), Location.TABLE_HEADER: View(Position(1, 0))}))
def quantile(df, col_name):
    print('Get %s for column %s... '%(inspect.stack()[0][3], col_name), end='')
    if df[col_name].dtypes not in ["object"]:
        plt.figure(figsize=(10,2))
        sns.boxplot(data=df, x=col_name, color="#3283FE")
        plt.xlabel(""); plt.ylabel(""); plt.show()
    print('Done!')

@register_udf(Config("missing values", OutputType.IMAGE,
                     {Location.SUMMARY: View(Position(0, 2), Shape(200, 50))}))
def missing_value(df, col_name):
    print('Get %s for column %s... '%(inspect.stack()[0][3], col_name), end='')
    if str(type(df)) == "<class 'cnextlib.dataframe.DataFrame'>":
        tmp_df = df.df
    else:
        tmp_df = df
    na_values = tmp_df[[col_name]].isna()
    if na_values.sum()[col_name] > 0:
        plt.figure(figsize=(5,0.5))
        res = sns.heatmap(data=na_values.transpose(), fmt="g", 
                          cmap=sns.color_palette("Blues_r"), 
                          yticklabels=False, xticklabels=False, cbar=False) 
        for _, spine in res.spines.items():
            spine.set_visible(True)
            spine.set_linewidth(0.5)
        plt.show()   
    print('Done!')

@register_udf(Config("violin_plot", OutputType.IMAGE, 
                     {Location.TABLE_HEADER: View(Position(2, 0)),
                     Location.SUMMARY: View(Position(0, 2))}))
def violin_plot(df, col_name):
    print('Get %s for column %s... '%(inspect.stack()[0][3], col_name), end='')
    if df[col_name].dtypes not in ["object"]:
        ar = df[col_name]
        sns.violinplot(data=ar)
        plt.show()
    else:
        return None
    print('Done!')

from cnextlib.udf_manager import register_udf, Config, View, OutputType, Position, Shape, Location, View, clear_udfs
import plotly.express as px
import plotly.io as pio
pio.renderers.default = "jupyterlab"
import inspect
MAX_POINT_COUNT = 1000

@register_udf(Config("histogram_plotly", OutputType.IMAGE, 
                     {Location.TABLE_HEADER: View(Position(1, 0)), 
                      Location.SUMMARY: View(Position(0, 1))}))
def histogram_plotly(df, col_name):
    print('Get %s for column %s... '%(inspect.stack()[0][3], col_name))
    if df[col_name].dtypes not in ["object"]:
        if df.shape[0] > MAX_POINT_COUNT:
            tmp_df = df.sample(MAX_POINT_COUNT)
        else:
            tmp_df = df
        fig = px.histogram(tmp_df, x=col_name)
    else:
        fig = px.bar(df[col_name].value_counts()[:])
        
    fig.update_layout({
        'showlegend': False,
        'margin': {'b': 0, 'l': 0, 'r': 0, 't': 0}, 
        'xaxis': {'showticklabels': False},
        'yaxis': {'showticklabels': False},
        'hoverlabel': {
            'bgcolor': "rgba(0,0,0,0.04)", 
            'bordercolor': "rgba(0,0,0,0.04)", 
            'font': {'color': "rgba(0,0,0,0.6)", 'size': 12 }
    }})
    
    fig.update_yaxes(visible=False, showticklabels=False)
    fig.update_xaxes(visible=False, showticklabels=False)
    fig.show()
    print('Done!')

@register_udf(Config("quantile_plotly", OutputType.IMAGE, {Location.TABLE_HEADER: View(Position(1, 0)), Location.SUMMARY: View(Position(0, 1))}))
def quantile_plotly(df, col_name):
    print('Get %s for column %s... '%(inspect.stack()[0][3], col_name))
    if df[col_name].dtypes not in ["object"]:
        if df.shape[0] > MAX_POINT_COUNT:
            tmp_df = df.sample(MAX_POINT_COUNT)
        else:
            tmp_df = df
        fig = px.box(tmp_df, x=col_name)
        fig.update_layout({
            'showlegend': False,
            #'width': 600, 
            #'height': 400, 
            'margin': {'b': 0, 'l': 0, 'r': 0, 't': 0}, 
            'xaxis': {'showticklabels': False},
            'yaxis': {'showticklabels': False},
            'hoverlabel': {
                'bgcolor': "rgba(0,0,0,0.04)", 
                'bordercolor': "rgba(0,0,0,0.04)", 
                'font': {'color': "rgba(0,0,0,0.6)", 'size': 12 }
        }})    
        fig.update_yaxes(visible=False, showticklabels=False)
        fig.update_xaxes(visible=False, showticklabels=False)
        fig.show()
    else:
        return None
    print('Done!')